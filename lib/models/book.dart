import 'package:cloud_firestore/cloud_firestore.dart';

class Book {
  Book(DocumentSnapshot doc) {
    this.author = doc['author'];
    this.surname = doc['surname'];
    this.title = doc['title'];
    this.hasMp3 = doc['has_mp3'] == null ? false : doc['has_mp3'];
    this.recordUrl = doc['record_url'];
  }

  String author;
  String title;
  String surname;
  bool hasMp3;
  String recordUrl;
}
