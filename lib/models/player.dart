// player.dart

import 'package:flutter/material.dart';

class PlayerModel with ChangeNotifier {
  double _time = 0;
  bool _playing = false;
  final int _songDuration = 100;

  get time => _time;

  set time(double timeInMiliseconds) {
    _time = timeInMiliseconds;
    notifyListeners();
  }

  bool get isPlaying => _playing;

  pause() {
    _playing = false;
    notifyListeners();
  }

  play() {
    _playing = true;
    notifyListeners();
  }

  int get songDuration => _songDuration;
}
