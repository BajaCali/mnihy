// styles.dart

import 'package:flutter/material.dart';



//make class for these things
const LargeTextSize = 26.0;
const MediumTextSize = 20.0;
const BodyTextSize = 16.0;
//

const String FontNameDefault = 'Montserrat';

///Defined colors in App
class AppColors {
  static const Color colorAccent = const Color(0xFF56297A); // violet
  static const Color colorDeccent = Color(0xFFD487D6); // pink
  static const Color colorDark = Color(0xFF524B49); // gray
  static const Color colorLite = Color(0xFFF0ECDC); // creme
  static const Color colorSecondary = Color(0xFF81CEC0); // turquoise
}

///Text style - AppBar
const AppBarTextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontWeight: FontWeight.w400,
  fontSize: MediumTextSize,
  color: AppColors.colorAccent,
);

///Text style - Title
const TitleTextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontWeight: FontWeight.w300,
  fontSize: LargeTextSize,
  color: AppColors.colorLite,
);

///Text style - Body 1
const Body1TextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontWeight: FontWeight.w300,
  fontSize: BodyTextSize,
  color: Colors.black,
);
