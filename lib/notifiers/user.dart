// user.dart

import 'package:Mnihy/models/user.dart';
import 'package:flutter/foundation.dart';

class UserNotifier extends ChangeNotifier {
  UserModel user = UserModel();

  bool get isLogedIn => user.isLogedIn();

  login() {
    user.login();
    notifyListeners();
  }
}
