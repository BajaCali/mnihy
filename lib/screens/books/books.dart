// books.dart

import 'package:Mnihy/app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'book_list.dart';
import '../../styles.dart';

const String logOut = 'Odhlásit se';
const String feedback = 'Poslat nám zprávu';

/// PopOutMenuButton (on AppBar right) choices
const List<Choice> choices = const <Choice>[
  const Choice(title: logOut, icon: Icons.cancel),
  const Choice(title: feedback, icon: Icons.message),
];

///BUILD Screen of the LIST OF BOOKS
class Books extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BookList(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.colorDark,
      elevation: 10.0,
      automaticallyImplyLeading: false,
      title: Text(
        "Mnihy",
        style: AppBarTextStyle,
      ),
      actions: <Widget>[
        PopupMenuButton(
          onSelected: (Choice choice) {
            switch (choice.title) {
              case logOut:
                print('Log Out');
                FirebaseAuth.instance.signOut();
                Navigator.pushReplacementNamed(context, LoginSingUpRoute);
                break;
              case feedback:
                print('Feedback');
                _giveFeedback();
                break;
            }
          },
          itemBuilder: (BuildContext context) {
            return choices.map((Choice choice) {
              return PopupMenuItem<Choice>(
                value: choice,
                child: Row(
                  children: <Widget>[
                    Text(choice.title),
                    Expanded(
                      child: Container(),
                    ),
                    Icon(choice.icon),
                  ],
                ),
              );
            }).toList();
          },
        )
      ],
    );
  }

  Future<void> _giveFeedback() async {
    String feedbackUrl = 'mailto:audiomaturita@gmail.com?subject=Feedback';
    if (await canLaunch(feedbackUrl)) {
      await launch(feedbackUrl);
    } else {
      print('Could not launch $feedbackUrl');
    }
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}
