// book_card.dart

import 'package:Mnihy/services/book_image.dart';

import '../../models/book.dart';
import 'package:flutter/material.dart';

import '../../app.dart';
import '../../styles.dart';
import 'marquee_widget.dart';

///CONSTANTS
const _cardHeight = 50.0;
const _cardBorderRadius = 10.0;

class BookCard extends StatelessWidget {
  final Book book;
  BookCard(this.book);

  @override
  Widget build(BuildContext context) {
    return Card(
      ///Set up
      color: AppColors.colorDark,
      elevation: 10.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(_cardBorderRadius)),

      child: InkWell(
        //tapping
        onTap: () => (book.hasMp3)
            ? Navigator.pushNamed(context, PlayerRoute,
                arguments: {'book': book})
            : null,
        child: new CardDataWidget(book),
      ),
    );
  }
}

class CardDataWidget extends StatelessWidget {
  final Book book;
  const CardDataWidget(this.book);

  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
        CardImageIcon(book),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TitleNameText(book),

            //TODO: Následující Row předělat
            Row(
              children: <Widget>[
                new AuthorNameBox(book),
                new IsPlayableIcon(book: book),
                //jiný způsob argumentu - example DONT DELET PLS
              ],
            )
          ],
        ),
      ],
    );
  }
}

///Set up the part in a card for Author Name
class AuthorNameBox extends StatelessWidget {
  final Book book;
  const AuthorNameBox(this.book);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 104.0,
      child: new Text(
        book.author,
        style: Theme.of(context).textTheme.subtitle,
      ),
    );
  }
}

///part in a card for ISPlayeable Icon (right bottom corner)
class IsPlayableIcon extends StatelessWidget {
  const IsPlayableIcon({
    Key key,
    @required this.book,
  }) : super(key: key);

  final Book book;

  @override
  Widget build(BuildContext context) {
    return Icon(
      Icons.play_arrow,
      color: (book.hasMp3) ? AppColors.colorAccent : Colors.transparent,
    );
  }
}

///part in a card for CardImage Icon (Left side)
class CardImageIcon extends StatelessWidget {
  final Book _book;
  CardImageIcon(this._book);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: new EdgeInsets.all(5.0),
      height: _cardHeight,
      width: _cardHeight,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
        child: BookImage(_book),
      ),
    );
  }
}

///part in a card for Title Name (top side)
class TitleNameText extends StatelessWidget {
  final Book book;
  TitleNameText(this.book);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 90.0,

      //Co dělá MarqueeWidget???
      child: MarqueeWidget(
        child: Text(
          book.title,
          style: Theme.of(context).textTheme.title,
        ),
        direction: Axis.horizontal,
        animationDuration:
            Duration(milliseconds: (400 * (book.title.length - 17))),
        backDuration: Duration(milliseconds: (400 * (book.title.length - 17))),
      ),
    );
  }
}
