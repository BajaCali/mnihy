import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../models/book.dart';
import 'book_card.dart';
import '../../styles.dart';

class BookList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream:
          Firestore.instance.collection('books').orderBy('surname').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) return new Text('Error: ${snapshot.error}');

        /// switch - waiting for connection to server
        ///         - Make screen
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return new Text('Loading...');
            break;

          ///Theme of body
          default:
            return new LBooksBodyTheme(snapshot);
        }
      },
    );
  }
}

class LBooksBodyTheme extends StatelessWidget {
  final AsyncSnapshot<QuerySnapshot> snapshot;
  const LBooksBodyTheme(this.snapshot);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(accentColor: AppColors.colorAccent),

      ///child: list of books
      child: new ListView(
        children: snapshot.data.documents.map((DocumentSnapshot document) {
          ///Single Card
          return new Container(
              margin: EdgeInsets.symmetric(horizontal: 6, vertical: 3),
              child: BookCard(Book(document)));
        }).toList(),
      ),
    );
  }
}
