import 'package:Mnihy/models/book.dart';
import 'package:Mnihy/services/book_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoverImage extends StatelessWidget {
  final Book _book;

  CoverImage(this._book);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BookImage(_book),
      margin: EdgeInsets.all(10.0),
    );
    //   if (_book != null) {
    //     return Container(
    //         margin: EdgeInsets.all(10.0),
    //         decoration: BoxDecoration(color: Colors.blueGrey),
    //         child: Image.network(_book));
    //   }
    //   return Container(
    //       margin: EdgeInsets.all(20.0),
    //       child: Image.asset('assets/images/book_icon.png'));
  }
}
