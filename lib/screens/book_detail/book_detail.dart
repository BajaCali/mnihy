// book_detail.dart

import 'package:flutter/cupertino.dart';

import '../../app.dart';
import 'package:flutter/material.dart';
import '../../models/book.dart';
import 'cover_image.dart';
// import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class BookDetail extends StatelessWidget {
  final Book book;

  BookDetail(this.book);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(book.title),
      ),
      body: ListView(
        children: <Widget>[
          CoverImage(book),
          Text(book.author),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow),
        onPressed: () => Navigator.pushNamed(context, PlayerRoute,
            arguments: {'book': book}),
      ),
    );
  }
}
