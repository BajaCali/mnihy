import 'package:Mnihy/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../styles.dart';

const String subscriptionId = 'subscription_v1';

class LoginSignUpPage extends StatefulWidget {
  @override
  _LoginSignUpPageState createState() => _LoginSignUpPageState();
}

class _LoginSignUpPageState extends State<LoginSignUpPage> {
  var _isLoading = true;
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  GoogleSignInAccount googleAccount;
  FirebaseUser _user;
  String message = 'Staň se hrdinou své maturity!';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              _showLogo(),
              _showMessage(),
              _showSignUpButton()
            ],
          ),
        ),
        padding: EdgeInsets.all(16.0),
      ),
    );
  }

  Widget _showSignUpButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: AppColors.colorLite,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  child: Image.asset('assets/images/google_logo_G.png'),
                  padding: EdgeInsets.all(5.0),
                ),
                Text('Vstoupit', style: AppBarTextStyle),
              ],
            ),
            onPressed: () => _btnPressed(),
          ),
        ));
  }

  Widget _showLogo() {
    return new Padding(
      padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 15.0),
      child: Image.asset('assets/images/logo_wide.png'),
    );
  }

  Widget _showMessage() {
    if (message.length > 0 && message != null) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 60.0),
        child: Center(
          child: new Text(
            message,
            style: AppBarTextStyle,
          ),
        ),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _initUser();
  }

  Future<Null> _initUser() async {
    if ((_user = await FirebaseAuth.instance.currentUser()) != null) {
      print('Signed in: ' + _user.displayName);
      Navigator.pushReplacementNamed(context, BooksRoute);
    }
  }

  _btnPressed() async {
    if (googleSignIn.currentUser == null)
      googleAccount = await googleSignIn.signIn();
    print('Google User: ' + googleAccount.displayName);
    _user = await signIntoFirebase(googleAccount);

    Firestore.instance.collection('users').add({
      'email': _user.email,
      'name': _user.displayName,
      'id': _user.uid,
      'timestamp': _user.metadata.creationTime.millisecondsSinceEpoch,
      'photoUrl': _user.photoUrl
    });

    print(_user.metadata.creationTime);
    Navigator.pushReplacementNamed(context, BooksRoute);
  }
}

Future<FirebaseUser> signIntoFirebase(
    GoogleSignInAccount googleSignInAccount) async {
  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignInAuthentication googleAuth =
      await googleSignInAccount.authentication;
  AuthCredential cred = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
  return (await _auth.signInWithCredential(cred)).user;
}
