import 'package:Mnihy/services/book_image.dart';
import 'package:Mnihy/styles.dart';
import 'package:flutter/material.dart';

import 'player_widget.dart';
import '../../models/book.dart';

class Player extends StatelessWidget {
  final Book _book;

  Player(this._book);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(_book.title),
          elevation: 0,
          backgroundColor: AppColors.colorDark,
        ),
        backgroundColor: AppColors.colorDark,
        body: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
          width: MediaQuery.of(context).size.width - 20.0,
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width - 20.0,
                  width: MediaQuery.of(context).size.width - 20.0,
                  child: BookImage(_book),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text(
                        _book.author,
                        style: TitleTextStyle,
                      ),
                      PlayerWidget(
                        book: _book,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
