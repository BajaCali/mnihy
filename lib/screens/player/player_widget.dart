import 'dart:typed_data';
import 'dart:io';
import 'dart:async';
import 'dart:math';

import 'package:Mnihy/models/book.dart';
import 'package:Mnihy/styles.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

///FLOATING BTN widget _btn - line 195 uncomment press and mp3 download to cache

enum PlayerState { stopped, playing, paused }

const backwardSeconds = 30;
const String p =
    "https://firebasestorage.googleapis.com/v0/b/mnihy-database.appspot.com/o/Records%2F1984.mp3?alt=media&token=819c2f2f-73aa-400f-a912-fff848070c7a";
const forwardSeconds = 30;

class PlayerWidget extends StatefulWidget {
  final bool isLocal;
  final PlayerMode mode;
  final Book book;

  PlayerWidget(
      {@required this.book,
      this.isLocal = false,
      this.mode = PlayerMode.MEDIA_PLAYER});

  @override
  State<StatefulWidget> createState() {
    return new _PlayerWidgetState(book, isLocal, mode);
  }
}

class _PlayerWidgetState extends State<PlayerWidget> {
  String url;
  bool isLocal;
  PlayerMode mode;
  Book book;

  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;

  PlayerState _playerState = PlayerState.stopped;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;

  get _isPlaying => _playerState == PlayerState.playing;
  // get _isPaused => _playerState == PlayerState.paused;
  get _durationText => _duration?.toString()?.split('.')?.first ?? '';
  get _positionText => _position?.toString()?.split('.')?.first ?? '';

  _PlayerWidgetState(this.book, this.isLocal, this.mode);

  @override
  void initState() {
    super.initState();
    _initAudioPlayer();
  }

  File _catchedFile;
  String _path;

  //TODO: delete printers
  Future<Null> downloadFile(String httpPath) async {
    print(httpPath);
    final RegExp regExp = RegExp('([^?/]*\.(mp3))');
    final String fileName = regExp.stringMatch(httpPath);
    final String fileName2 = (book.title + '.mp3');
    print(fileName);
    print(fileName2);
    final Directory tempDir = Directory.systemTemp;
    final File file = File('${tempDir.path}/$fileName2');
    print(file.path);
    final StorageReference ref =
        FirebaseStorage.instance.ref().child("Records/" + book.title + '.mp3');
    final StorageFileDownloadTask downloadTask = ref.writeToFile(file);
    final int byteNumber = (await downloadTask.future).totalByteCount;
    print(byteNumber);
  }

  @override
  void dispose() {
    _audioPlayer.stop();
    _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    _playerErrorSubscription?.cancel();
    _playerStateSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _sliderContainer(),
            _durPosTexts(),
            _playerCtrlBtns(),
          ],
        ),
      ],
    );
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    _audioPlayer.seek(newDuration);
  }

  ///duration position TextFields
  Widget _durPosTexts() {
    return new Row(
      children: <Widget>[
        Text(
          _position != null ? '${_positionText ?? ''}' : '00:00',
          style: TextStyle(color: AppColors.colorLite),
        ),
        Expanded(
          child: Container(),
        ),
        Text(
          _duration != null ? '${_durationText ?? ''}' : '00:00',
          style: TextStyle(color: AppColors.colorLite),
        ),
      ],
    );
  }

  Widget _sliderContainer() {
    return new Container(
      height: 20,
      width: MediaQuery.of(context).size.width + 40,
      child: Slider(
        activeColor: AppColors.colorLite,
        inactiveColor: AppColors.colorLite,
        value: _position != null ? _position.inMilliseconds.toDouble() : 0.0,
        max: _duration != null ? _duration.inMilliseconds.toDouble() : 0.0,
        min: 0,
        onChanged: (double value) {
          _audioPlayer.seek(Duration(milliseconds: value.toInt()));
        },
      ),
    );
  }

  Widget _btn() {
    return new RawMaterialButton(
        shape: new CircleBorder(),
        fillColor: AppColors.colorLite,
        splashColor: AppColors.colorDark,
        highlightColor: AppColors.colorDark.withOpacity(0.5),
        elevation: 10.0,
        highlightElevation: 5.0,
        onPressed: () async {
          //await downloadFile(book.recordUrl);
          await downloadFile(p);
        });
  }

  ///backward, playPause, forward
  Widget _playerCtrlBtns() {
    return new Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _backwardBtn(),
        _playPauseBtn(),
        _forwardBtn(),
        //_btn(),                 //FLOATING BTN - press and mp3 download then
      ],
    );
  }

  Widget _playPauseBtn() {
    return new RawMaterialButton(
      shape: new CircleBorder(),
      fillColor: AppColors.colorLite,
      splashColor: AppColors.colorDark,
      highlightColor: AppColors.colorDark.withOpacity(0.5),
      elevation: 10.0,
      highlightElevation: 5.0,
      onPressed: _isPlaying ? () => _pause() : () => _play(),

      //Play Icon
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(
          _isPlaying ? Icons.pause : Icons.play_arrow,
          color: AppColors.colorAccent,
          size: 50.0,
        ),
      ),
    );
  }

  Widget _backwardBtn() {
    return new IconButton(
      onPressed: () {
        backwardSeconds * 1000 < _position.inMicroseconds
            ? _audioPlayer.seek(Duration(
                milliseconds:
                    _position.inMilliseconds - backwardSeconds * 1000))
            : _audioPlayer.seek(Duration(milliseconds: 0));
        print('Fast Rewind pressed.');
        print(_position.inMilliseconds);
      },
      padding: EdgeInsets.only(left: 0.0),
      iconSize: 50.0,
      highlightColor: Colors.black,
      icon: Icon(
        Icons.fast_rewind,
        color: AppColors.colorLite,
      ),
    );
  }

  Widget _forwardBtn() {
    return new IconButton(
      onPressed: () {
        _duration.inMilliseconds - forwardSeconds * 1000 >
                _position.inMilliseconds
            ? _audioPlayer.seek(Duration(
                milliseconds: _position.inMilliseconds + forwardSeconds * 1000))
            : _audioPlayer.seek(_duration);
        print('Fast Forward pressed.');
      },
      padding: EdgeInsets.only(left: 0.0),
      iconSize: 50.0,
      highlightColor: Colors.black,
      icon: Icon(
        Icons.fast_forward,
        color: AppColors.colorLite,
      ),
    );
  }

  Future _initAudioPlayer() async {
    _audioPlayer = AudioPlayer(mode: mode);

    _durationSubscription =
        _audioPlayer.onDurationChanged.listen((duration) => setState(() {
              _duration = duration;
            }));

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = _duration;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    await _fetch(book);
    print('Fetched.');
    await _play(); // Failing here.
    print('Played.');
    await _pause();
    print('Paused.');
  }

  Future<int> _play() async {
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result =
        await _audioPlayer.play(url, isLocal: isLocal, position: playPosition);
    if (result == 1) setState(() => _playerState = PlayerState.playing);
    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  // Future<int> _stop() async {
  //   final result = await _audioPlayer.stop();
  //   if (result == 1) {
  //     setState(() {
  //       _playerState = PlayerState.stopped;
  //       _position = Duration();
  //     });
  //   }
  //   return result;
  // }

  Future<void> _fetch(Book book) async {
    final ref = FirebaseStorage.instance.ref();
    try {
      ref
          .child('Records')
          .child(book.title + '.mp3')
          .getDownloadURL()
          .then((v) {
        setState(() {
          url = v.toString();
        });
      });
    } catch (e) {
      print('Book doesn\'t have a record. Error: $e');
    }
  }

  void _onComplete() {
    setState(() => _playerState = PlayerState.stopped);
  }
}
