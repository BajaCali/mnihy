// app.dart
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'screens/book_detail/book_detail.dart';
import 'screens/books/books.dart';
import 'screens/login_singup/login_singup.dart';
import 'screens/player/player.dart';
import 'styles.dart';
import 'notifiers/user.dart';

const BooksRoute = '/books';
const BookDetailRoute = '/book_detail';
const PlayerRoute = '/player';
const LoginSingUpRoute = '/';

/// APP builder
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      child: MaterialApp(
        title: 'Mnihy',
        onGenerateRoute: _routes(),
        theme: _themeData(),
      ),
      builder: (BuildContext context) => UserNotifier(),
    );
  }

  /// screen switcher
  RouteFactory _routes() {
    return (settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;
      switch (settings.name) {
        case LoginSingUpRoute:
          screen = LoginSignUpPage();
          break;

        case BooksRoute:
          screen = Books();
          break;

        case BookDetailRoute:
          screen = BookDetail(arguments['book']);
          break;

        case PlayerRoute:
          screen = Player(arguments['book']);
          break;

        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }

  ///Define basic Theme
  ThemeData _themeData() {
    return ThemeData(
        appBarTheme: AppBarTheme(textTheme: TextTheme(title: AppBarTextStyle)),
        textTheme: TextTheme(
          title: TitleTextStyle,
          body1: Body1TextStyle,
        ));
  }
}
