// book_image.dart

import 'package:Mnihy/models/book.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

enum BookImageState { fetching, fetched }

class BookImage extends StatefulWidget {
  final Book _book;
  const BookImage(this._book);
  @override
  State<StatefulWidget> createState() {
    return new _BookImageState(_book);
  }
}

class _BookImageState extends State<BookImage> {
  final Book _book;
  _BookImageState(this._book);

  Image _image;
  BookImageState _bookImageState;

  @override
  void initState() {
    _bookImageState = BookImageState.fetching;
    _fetch(_book);
    super.initState();
  }

  ///Add book_icon - if !exists -> put default mnihy Icon
  @override
  Widget build(BuildContext context) {
    if (_bookImageState == BookImageState.fetching) {
      print('Asset');
      return Image.asset('assets/images/default_book_icon.png');
    } else {
      print('URL');
      return _image;
    }
  }

  Future<void> _fetch(Book book) async {
    final ref = FirebaseStorage.instance.ref();
    try {
      ref.child('Images').child(book.title + '.png').getDownloadURL().then((v) {
        _loadImage(v.toString());
      });
    } catch (e) {
      print('Book doesn\'t have an image. Error: $e');
    }
  }

  Future<void> _loadImage(String url) async {
    _image = new Image.memory(await http.readBytes(url));
    setState(() {
      _bookImageState = BookImageState.fetched;
    });
  }
}
